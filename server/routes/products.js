const express = require('express');
const mongoose = require('mongoose');
const Product = require('../models/product');
const _ = require('underscore');
const { validateToken } = require('../middlewares/authentication');

const app = express();


app.get('/products', validateToken, function(req, res) {
    let from = req.query.from || 0;
    from = Number(from);

    let limit = req.query.limit || 5;
    limit = Number(limit);

    let filter = {
        status: true
    }

    Product.find(filter, 'name price description available')
        .populate('user', 'name email')
        .populate('category', 'description')
        .sort('description')
        .skip(from)
        .limit(limit)
        .exec(async(err, products) => {

            if (err) return res.status(500).json({ message: 'Error de base de datos.', err });

            let count = await Product.countDocuments(filter)

            res.json({ products, count });

        });
});

app.get('/products/search/:criteria', validateToken, function(req, res) {
    let criteria = req.params.criteria;

    let regex = new RegExp(criteria, 'i');

    Product.find({ name: regex })
        .populate('user', 'name email')
        .populate('category', 'description')
        .exec(async(err, products) => {

            if (err) return res.status(500).json({ message: 'Error de base de datos.', err });

            let count = await Product.countDocuments({})

            res.json({ products, count });

        });
});

app.post('/products', validateToken, function(req, res) {
    let product = new Product({
        name: req.body.name,
        price: req.body.price,
        description: req.body.description,
        user: mongoose.Types.ObjectId(req.user._id),
        category: mongoose.Types.ObjectId(req.body.categoryId),
    });

    product.save((err, productDB) => {

        if (err) return res.status(500).json({ message: 'Error de base de datos.', err });

        res.status(201)
            .json({
                message: 'Producto registrado.',
                product: productDB
            });

    });
});

app.get('/products/:id', validateToken, function(req, res) {
    Product.findById(req.params.id)
        .populate('user', 'name email')
        .populate('category', 'description')
        .exec((err, productDB) => {

            if (err) return res.status(500).json({ message: 'Error de base de datos.', err });
            if (!productDB) return res.status(400).json({ message: 'Producto no encontrado.', err: { message: 'Data Not Found.' } });
            if (!productDB.status) return res.status(400).json({ message: 'Producto no encontrado.', err: { message: 'Data Not Found.' } });

            res.status(200)
                .json({ product: productDB });

        });
});

app.patch('/products/:id', validateToken, function(req, res) {
    let id = req.params.id;
    let body = _.pick(req.body, ['name', 'price', 'description', 'category']);

    Product.findByIdAndUpdate(id, body, { new: true, runValidators: true, context: 'query' }, (err, productDB) => {

        if (err) return res.status(500).json({ message: 'Error de base de datos.', err });
        if (!productDB) return res.status(400).json({ message: 'Producto no encontrado.', err: { message: 'Data Not Found.' } });

        res.json({ product: productDB });

    });
});

app.delete('/products/:id', validateToken, function(req, res) {
    let id = req.params.id;

    Product.findByIdAndUpdate(id, { status: false }, { new: true, runValidators: true, context: 'query' }, (err, productDB) => {

        if (err) return res.status(500).json({ message: 'Error de base de datos.', err });
        if (!productDB) return res.status(400).json({ message: 'Producto no encontrado.', err: { message: 'Data Not Found.' } });

        res.json({ product: productDB });

    });
});

module.exports = app;