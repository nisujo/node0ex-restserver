const express = require('express');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../models/user');

const app = express();


app.post('/login', (req, res) => {

    User.findOne({ email: req.body.email }, (err, userDB) => {

        if (err) {

            return res.status(500).json({ message: 'Error de base de datos.', err });

        } else if (!userDB) {

            return res.status(400).json({ message: '(Usuario) o contraseña incorrectos.', err: { message: 'Credentials error.' } });

        } else if (!bcrypt.compareSync(req.body.password, userDB.password)) {

            return res.status(400).json({ message: 'Usuario o (contraseña) incorrectos.', err: { message: 'Credentials error.' } });

        }

        let token = jwt.sign({
            user: userDB
        }, process.env.JWT_SEED, {
            expiresIn: process.env.JWT_EXPIRE
        });

        res.json({
            token,
            userDB
        });

    });

});


module.exports = app;