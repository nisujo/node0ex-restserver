const express = require('express');
const fileUpload = require('express-fileupload');
const fs = require('fs');
const path = require('path');
const Product = require('../models/product');
const User = require('../models/user');
const { validateToken } = require('../middlewares/authentication');

const app = express();


app.use(fileUpload({
    useTempFiles: true,
    tempFileDir: '/tmp/'
}));


app.put('/upload/:type/:id', validateToken, async function(req, res) {
    let availableTypes = ['users', 'products'];
    let type = req.params.type;
    let id = req.params.id;

    if (availableTypes.indexOf(type) == -1) {

        return res.status(400).json({ message: 'Tipo de archivo no válido', err: { message: 'File type invalid.' } });

    } else if (!req.files || Object.keys(req.files).length === 0) {

        return res.status(400).json({ message: 'No ha seleccionado archivo.', err: { message: 'File not found.' } });

    }

    let archive = req.files.archive;
    let randomName = generateRandomFilename(archive.name);
    let basePath = `uploads/${type}`;
    let uploadPath = `${basePath}/${randomName}`;
    let availableExtensions = ['image/png', 'image/jpeg', 'image/gif'];

    if (availableExtensions.indexOf(archive.mimetype) == -1) {

        return res.status(400).json({
            message: `Archivo de tipo ${archive.mimetype} no permitido`,
            err: {
                message: `Mimetype ${archive.mimetype} not allowed.`
            }
        });

    }

    archive.mv(uploadPath, async function(err) {

        if (err) return res.status(500).json({ message: 'No se pudo guardar el archivo.', err });

        if (type === 'users') {

            // Borrar imagen actual.
            let currentUserImage = await getUserImage(id);

            if (currentUserImage) {
                deleteFile(`${basePath}/${currentUserImage}`);
            }

            // Actualizar nueva imagen.
            User.findByIdAndUpdate(id, { img: randomName }, { new: true, runValidators: true, context: 'query' }, (err, userDB) => {

                if (err) return res.status(500).json({ message: 'Error de base de datos.', err });
                if (!userDB) return res.status(400).json({ message: 'Usuario no encontrado.', err: { message: 'Data Not Found.' } });

                res.json({ user: userDB });

            });

        } else if (type === 'products') {

            // Borrar imagen actual.
            let currentProductImage = await getProductImage(id);

            if (currentProductImage) {
                deleteFile(`${basePath}/${currentProductImage}`);
            }

            // Actualizar nueva imagen.
            Product.findByIdAndUpdate(id, { img: randomName }, { new: true, runValidators: true, context: 'query' }, (err, productDB) => {

                if (err) return res.status(500).json({ message: 'Error de base de datos.', err });
                if (!productDB) return res.status(400).json({ message: 'Producto no encontrado.', err: { message: 'Data Not Found.' } });

                res.json({ product: productDB });

            });

        }

    });

});

async function getUserImage(userId) {
    try {

        let user = await User.findById(userId).exec();

        if (!user) return null;

        return user.img || null;

    } catch (err) {

        return null;

    }
}

async function getProductImage(productId) {
    try {

        let product = await Product.findById(productId).exec();

        if (!product) return null;

        return product.img || null;

    } catch (err) {

        return null;

    }
}

function deleteFile(pathFile) {
    let deletePath = path.resolve(__dirname, '../../' + pathFile);

    if (fs.existsSync(deletePath)) {

        fs.unlinkSync(deletePath);

    }
}

function generateRandomFilename(filename) {
    let ext = filename.split('.').reverse()[0];
    let randomName = `${generateRandomString(26)}-${new Date().getTime()}.${ext}`;

    return randomName;
}

function generateRandomString(length) {
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    let result = '';

    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
}

module.exports = app;