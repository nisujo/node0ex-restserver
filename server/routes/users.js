const express = require('express');
const bcrypt = require('bcrypt');
const User = require('../models/user');
const _ = require('underscore');
const { validateToken, validateAdminAuthentication } = require('../middlewares/authentication');

const app = express();

app.get('/users', validateToken, function(req, res) {
    let from = req.query.from || 0;
    from = Number(from);

    let limit = req.query.limit || 5;
    limit = Number(limit);

    let filter = {
        status: true
    }

    User.find(filter, "name email role status google img")
        .skip(from)
        .limit(limit)
        .exec(async(err, users) => {

            if (err) return res.status(500).json({ message: 'Error de base de datos.', err });

            let count = await User.countDocuments(filter)

            res.json({ users, count });

        });
});

app.post('/users', [validateToken, validateAdminAuthentication], function(req, res) {
    if (req.body.name === undefined) {

        return res.status(500).json({ message: 'El nombre del usuario es obligatior.' });

    }

    let user = new User({
        name: req.body.name,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, 10),
        role: req.body.role,
    });

    user.save((err, userDB) => {

        if (err) return res.status(500).json({ message: 'Error de base de datos.', err });

        res.status(201)
            .json({
                message: 'Usuario registrado.',
                user: userDB
            });

    });
});

app.get('/users/:id', validateToken, function(req, res) {
    User.findById(req.params.id, (err, userDB) => {

        if (err) return res.status(500).json({ message: 'Error de base de datos.', err });
        if (!userDB) return res.status(400).json({ message: 'Usuario no encontrado.', err: { message: 'Data Not Found.' } });
        if (!userDB.status) return res.status(400).json({ message: 'Usuario no encontrado.', err: { message: 'Data Not Found.' } });

        res.status(200)
            .json({ user: userDB });

    });
});

app.patch('/users/:id', [validateToken, validateAdminAuthentication], function(req, res) {
    let id = req.params.id;

    req.body.password = bcrypt.hashSync(req.body.password, 10);

    let body = _.pick(req.body, ['name', 'email', 'img', 'role', 'status', 'password']);

    User.findByIdAndUpdate(id, body, { new: true, runValidators: true, context: 'query' }, (err, userDB) => {

        if (err) return res.status(500).json({ message: 'Error de base de datos.', err });
        if (!userDB) return res.status(400).json({ message: 'Usuario no encontrado.', err: { message: 'Data Not Found.' } });

        res.json({ user: userDB });

    });
});

app.delete('/users/:id', [validateToken, validateAdminAuthentication], function(req, res) {
    let id = req.params.id;

    User.findByIdAndUpdate(id, { status: false }, { new: true, runValidators: true, context: 'query' }, (err, userDB) => {

        if (err) return res.status(500).json({ message: 'Error de base de datos.', err });
        if (!userDB) return res.status(400).json({ message: 'Usuario no encontrado.', err: { message: 'Data Not Found.' } });

        res.json({ user: userDB });

    });
});

module.exports = app;