const express = require('express');
const fs = require('fs');
const path = require('path');
const { validateImageToken } = require('../middlewares/authentication');

const app = express();


app.get('/images/:type/:img', validateImageToken, (req, res) => {
    let availableTypes = ['users', 'products'];
    let type = req.params.type;
    let img = req.params.img;
    let noImageFound = path.resolve(__dirname, '../assets/no-image.jpg');

    if (availableTypes.indexOf(type) == -1) {

        return res.sendFile(noImageFound);

    }

    let fullPathImage = path.resolve(__dirname, `../../uploads/${type}/${img}`);

    if (!fs.existsSync(fullPathImage)) {

        return res.sendFile(noImageFound);

    }

    res.sendFile(fullPathImage);
});


module.exports = app;