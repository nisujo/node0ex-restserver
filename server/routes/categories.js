const express = require('express');
const mongoose = require('mongoose');
const Category = require('../models/category');
const _ = require('underscore');
const { validateToken, validateAdminAuthentication } = require('../middlewares/authentication');

const app = express();


app.get('/categories', validateToken, function(req, res) {
    let from = req.query.from || 0;
    from = Number(from);

    let limit = req.query.limit || 5;
    limit = Number(limit);

    let filter = {}

    Category.find(filter, "description")
        .populate('user', 'name email')
        .sort('description')
        .skip(from)
        .limit(limit)
        .exec(async(err, categories) => {

            if (err) return res.status(500).json({ message: 'Error de base de datos.', err });

            let count = await Category.countDocuments(filter)

            res.json({ categories, count });

        });
});

app.post('/categories', [validateToken, validateAdminAuthentication], function(req, res) {
    let category = new Category({
        description: req.body.description,
        user: mongoose.Types.ObjectId(req.user._id),
    });

    category.save((err, categoryDB) => {

        if (err) return res.status(500).json({ message: 'Error de base de datos.', err });

        res.status(201)
            .json({
                message: 'Categoría registrada.',
                category: categoryDB
            });

    });
});

app.get('/categories/:id', validateToken, function(req, res) {
    Category.findById(req.params.id)
        .populate('user', 'name email')
        .exec((err, categoryDB) => {

            if (err) return res.status(500).json({ message: 'Error de base de datos.', err });
            if (!categoryDB) return res.status(400).json({ message: 'Categoría no encontrada.', err: { message: 'Data Not Found.' } });

            res.status(200)
                .json({ category: categoryDB });

        });
});

app.patch('/categories/:id', [validateToken, validateAdminAuthentication], function(req, res) {
    let id = req.params.id;

    let body = _.pick(req.body, ['description']);

    Category.findByIdAndUpdate(id, body, { new: true, runValidators: true, context: 'query' }, (err, categoryDB) => {

        if (err) return res.status(500).json({ message: 'Error de base de datos.', err });
        if (!categoryDB) return res.status(400).json({ message: 'Categoría no encontrada.', err: { message: 'Data Not Found.' } });

        res.json({ category: categoryDB });

    });
});

app.delete('/categories/:id', [validateToken, validateAdminAuthentication], function(req, res) {
    let id = req.params.id;

    Category.findByIdAndRemove(id, {}, (err, categoryDB) => {

        if (err) return res.status(500).json({ message: 'Error de base de datos.', err });
        if (!categoryDB) return res.status(400).json({ message: 'Categoría no encontrada.', err: { message: 'Data Not Found.' } });

        res.json({ category: categoryDB });

    });
});

module.exports = app;