const mongoose = require('mongoose');
const express = require('express');
const path = require('path');
require('./config');

const app = express();

app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded


// Configuración global de rutas
app.use(require('./routes/index'));


// Habilitar contenido estatico
app.use(express.static(path.resolve(__dirname, '../public')));



mongoose.connect(process.env.URLDB, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true }, (err, res) => {

    if (err) throw err;

    console.log("Database online.");

});



app.listen(process.env.PORT, () => console.log('Running...'));