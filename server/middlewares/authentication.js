const jwt = require('jsonwebtoken');
const User = require('../models/user');

let validateToken = (req, res, next) => {
    let token = req.get('token');

    jwt.verify(token, process.env.JWT_SEED, (err, decoded) => {

        if (err) return res.status(401).json({ message: 'Acceso no autorizado.', err });

        req.user = decoded.user;
        next();

    });
}

let validateAdminAuthentication = async(req, res, next) => {
    try {

        let user = await User.findById(req.user._id);

        if (!req.user || user.role != 'ADMIN_ROLE') {

            return res.status(401).json({ message: 'Acceso no válido.', err: { message: 'User has no access.' } });

        }

        next();

    } catch (err) {

        return res.status(500).json({ message: 'Error interno del servidor.', err });

    }
}

let validateImageToken = (req, res, next) => {
    let token = req.query.token;

    jwt.verify(token, process.env.JWT_SEED, (err, decoded) => {

        if (err) return res.status(401).json({ message: 'Acceso no autorizado.', err });

        req.user = decoded.user;
        next();

    });
}

module.exports = { validateToken, validateAdminAuthentication, validateImageToken };