/**
 * Port
 */
process.env.PORT = process.env.PORT || 3000;

/**
 * Environment
 */
process.env.NODE_ENV = process.env.NODE_ENV || 'dev';

/**
 * Database
 */
let urlDB;

if (process.env.NODE_ENV === 'dev') {

    urlDB = 'mongodb://192.168.0.20:27017/cafe';

} else {

    urlDB = `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASS}@${process.env.DB_HOST}/${process.env.DB_NAME}`;

}

process.env.URLDB = urlDB;

/**
 * JWT
 */
process.env.JWT_EXPIRE = '48h';
process.env.JWT_SEED = process.env.JWT_SEED || '0CnBdz6cuqUT4';