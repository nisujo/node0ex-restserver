const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

let Schema = mongoose.Schema;

let availableRoles = {
    values: ['ADMIN_ROLE', 'USER_ROLE'],
    message: '{value} no es un rol válido.'
}

let userSchema = new Schema({
    name: {
        type: String,
        required: [true, 'El nombre es obligatorio.']
    },
    email: {
        type: String,
        unique: true,
        required: [true, 'El correo es obligatorio.']
    },
    password: {
        type: String,
        required: [true, 'La contraseña es obligatoria.']
    },
    img: {
        type: String,
        required: false
    },
    role: {
        type: String,
        default: 'USER_ROLE',
        required: false,
        enum: availableRoles
    },
    status: {
        type: Boolean,
        default: true,
        required: false
    },
    google: {
        type: Boolean,
        defaul: false
    }
});

userSchema.methods.toJSON = function() {
    let user = this;
    let userObject = user.toObject();
    delete userObject.password;
    return userObject;
}

userSchema.plugin(uniqueValidator, { message: '{PATH} debe de ser único.' });

module.exports = mongoose.model('User', userSchema);